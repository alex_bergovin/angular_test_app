import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { catchError, filter, finalize, switchMap, take } from 'rxjs/operators';
import { AuthService } from '../../services/auth.service';
import { HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
	constructor(private authService: AuthService) {
	}

	intercept(req: HttpRequest<any>, next: HttpHandler): Observable<any> {
		const token = localStorage.getItem('access_token');
		return next.handle(this.addTokenToRequest(req, token));
	}

	private addTokenToRequest(request: HttpRequest<any>, token: string): HttpRequest<any> {
		const headers = {
			Authorization: `Bearer ${token}`,
			'Content-type': 'application/json'
		};
		return request.clone({ setHeaders: headers });
	}
}
