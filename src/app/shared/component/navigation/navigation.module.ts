import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NavigationComponent } from './navigation.component';

@NgModule({
	declarations: [
		NavigationComponent
	],
	imports: [
		CommonModule,
		FormsModule
	],
	providers: [],
	exports: [NavigationComponent],
})
export class NavigationModule {
}
