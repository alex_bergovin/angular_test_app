import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app.routing.module';
import { AuthPageModule } from './components/auth/auth.module';
import { ServicesModule } from './services/services.module';
import { DashboardPageModule } from './components/dashboard/dashboard.module';
import { MainPageModule } from './components/main-page.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './shared/interceptors/auth.interceptor';

@NgModule({
	declarations: [
		AppComponent
	],
	imports: [
		AppRoutingModule,
		RouterModule,
		BrowserModule,
		AuthPageModule,
		ServicesModule,
		MainPageModule
	],
	providers: [
		{
			provide: HTTP_INTERCEPTORS,
			useClass: AuthInterceptor,
			multi: true,
		}],
	bootstrap: [AppComponent]
})
export class AppModule {
}
