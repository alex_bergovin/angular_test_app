import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthPageRoutes } from './components/auth/auth.routing';
import { MainPageRoutes } from './components/main-page.routing';

const routes: Routes = [
  { path: '', redirectTo: 'auth', pathMatch: 'full' },
  ...AuthPageRoutes,
  ...MainPageRoutes
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule {
}
