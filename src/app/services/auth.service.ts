import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import jwt_decode from 'jwt-decode';

@Injectable()
export class AuthService {

	public isLoggedIn() {
		const parsedToken: any = this.parseToken;
		return parsedToken.id && parsedToken.email;
	}

	public parseToken() {
		const token = localStorage.getItem('access_token');

		if (!token) {
			return;
		}

		return jwt_decode(token);
	}

	public isUser(id: number) {
		return this.parseToken().id === id;
	}
}
