import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseHttpService } from './base.http.service';
import { Observable, of } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable()
export class AuthApiService extends BaseHttpService {
  private static loginUrl = environment.serverUrl + 'auth/signin';
  private static logupUrl = environment.serverUrl + 'auth/signup';

  constructor(protected http: HttpClient) {
    super(http);
  }

  public localSignIn(data): Observable<any> {
    return this.http.post(AuthApiService.loginUrl, data);
  }

  public localSignUp(data): Observable<any> {
    return this.http.post(AuthApiService.logupUrl, data);
  }
}
