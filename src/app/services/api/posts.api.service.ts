import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseHttpService } from './base.http.service';
import { Observable, of } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable()
export class PostsApiService extends BaseHttpService {
	private static postsUrl = environment.serverUrl + 'posts';

	constructor(protected http: HttpClient) {
		super(http);
	}

	public getAllPosts(query): Observable<any> {
		return this.http.get(PostsApiService.postsUrl, {params: query});
	}

	public savePost(message: string): Observable<any> {
		return this.http.post(PostsApiService.postsUrl, { message });
	}

	public getPostId(id: number): Observable<any> {
		return this.http.get(`${PostsApiService.postsUrl}/${id}`, { });
	}

	public deletePost(id: number): Observable<any> {
		return this.http.delete(`${PostsApiService.postsUrl}/${id}`, { });
	}

	public editPost(id: number, body): Observable<any> {
		return this.http.put(`${PostsApiService.postsUrl}/${id}`, body);
	}
}
