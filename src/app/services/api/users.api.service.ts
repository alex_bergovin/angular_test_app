import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseHttpService } from './base.http.service';
import { Observable, of } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable()
export class UsersApiService extends BaseHttpService {
	private static usersUrl = environment.serverUrl + 'users';

	constructor(protected http: HttpClient) {
		super(http);
	}

	public getAllUsers(query): Observable<any> {
		return this.http.get(UsersApiService.usersUrl, {params: query});
	}

	public deleteUser(id: number): Observable<any> {
		return this.http.delete(`${UsersApiService.usersUrl}/${id}`, {});
	}

	public editUser(id: number, data): Observable<any> {
		return this.http.put(`${UsersApiService.usersUrl}/${id}`, data);
	}
}
