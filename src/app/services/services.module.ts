import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { AuthApiService } from './api/auth.api.service';
import { AuthService } from './auth.service';
import { UsersApiService } from './api/users.api.service';
import { PostsApiService } from './api/posts.api.service';


@NgModule({
	imports: [HttpClientModule],
	declarations: [],
	exports: [],
	providers: [
		AuthApiService,
		AuthService,
		UsersApiService,
		PostsApiService
	]
})
export class ServicesModule {
}
