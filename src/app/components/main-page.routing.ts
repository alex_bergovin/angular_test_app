import { Routes } from '@angular/router';
import { MainPageComponent } from './main-page.component';
import { DashboardPageRoutes } from './dashboard/dashboard.routing';

export const MainPageRoutes: Routes = [
  { path: '', redirectTo: 'main', pathMatch: 'full' },
  {
    path: 'main', component: MainPageComponent, children: [
      ...DashboardPageRoutes
    ]
  }]
;
