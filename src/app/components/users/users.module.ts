import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UsersComponent } from './users.component';

@NgModule({
	declarations: [
		UsersComponent
	],
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
	],
	providers: [],
	exports: [UsersComponent]
})
export class UsersPageModule {
}
