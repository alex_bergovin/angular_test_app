import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { UsersApiService } from '../../services/api/users.api.service';
import { registerLocaleData } from '@angular/common';
import { AuthService } from '../../services/auth.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
	selector: 'app-users',
	templateUrl: './users.component.html',
	styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
	@Input() id: number;
	@Input() name: string;
	@Input() email: string;
	@Input() registeredAt: string;
	@Input() phone: string;
	@Output() deleteUserEvent = new EventEmitter();
	public isEdit: boolean;
	public form: FormGroup;

	constructor(public authService: AuthService, private userService: UsersApiService) { }

	ngOnInit() {
		this.initForm();
	}

	public onDelete() {
		this.userService.deleteUser(this.id)
			.subscribe(res => {
				this.deleteUserEvent.emit(this.id);
			});
	}

	public onEdit() {
		this.isEdit = true;
		this.updateForm();
	}

	private initForm() {
		this.form = new FormGroup({
			name: new FormControl('da9@tut.by', [Validators.required]),
			email: new FormControl('12345678', [Validators.required]),
			phone: new FormControl('12345678', [Validators.required])
		});
	}

	public updateForm() {
		this.form.patchValue({
			email: this.email,
			name: this.name,
			phone: this.phone,
		});
	}

	public edit() {
		this.userService.editUser(this.id, this.form.value)
			.subscribe(res => {
				this.isEdit =  false;
				this.email = this.form.value.email;
				this.phone = this.form.value.phone;
				this.name = this.form.value.name;
			});
	}

}
