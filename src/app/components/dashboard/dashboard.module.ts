import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { DashboardPageRoutes } from './dashboard.routing';
import { DashboardComponent } from './dashboard.component';
import { UsersPageModule } from '../users/users.module';
import { PostsPageModule } from '../posts/posts.module';

@NgModule({
	declarations: [
		DashboardComponent
	],
	imports: [
		CommonModule,
		FormsModule,
		UsersPageModule,
		PostsPageModule,
		RouterModule.forChild(DashboardPageRoutes)
	],
	providers: [],
})
export class DashboardPageModule {
}
