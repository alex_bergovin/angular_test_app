import { Component, OnInit } from '@angular/core';
import { UsersApiService } from '../../services/api/users.api.service';
import { Observable, BehaviorSubject } from 'rxjs';
import { map, take, switchMap } from 'rxjs/operators';
import { PostsApiService } from '../../services/api/posts.api.service';

@Component({
	selector: 'app-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
	// public users$: Observable<any>;
	// public posts$: Observable<any>;
	public currentMessage: string;

	private postsSubject: BehaviorSubject<any[]> = new BehaviorSubject<any>([]);
	private usersSubject: BehaviorSubject<any[]> = new BehaviorSubject<any>([]);
	public posts$: Observable<any> = this.postsSubject.asObservable();
	public users$: Observable<any> = this.usersSubject.asObservable();

	constructor(private userApiService: UsersApiService, private postsApiService: PostsApiService) {
		this.currentMessage = '';
		this.getPosts();
		this.getUsers();
	}

	ngOnInit() {
	}

	public getUsers(query = {}) {
		this.userApiService.getAllUsers(query)
			.pipe(
				map(el => el.data)
			).subscribe(data => this.usersSubject.next(data));
	}

	public getPosts(query = {}) {
		this.postsApiService.getAllPosts(query)
			.pipe(
				map(el => el.data)
			).subscribe(data => this.postsSubject.next(data));
	}

	public addNicknameToChat(nickname) {
		this.currentMessage = `${this.currentMessage} ${nickname},`;
	}

	public savePost() {
		this.postsApiService
			.savePost(this.currentMessage)
			.pipe(
				switchMap(res => this.postsApiService.getPostId(res.data.id)),
				map(postResponse => postResponse.data)
			).subscribe(post => {
				const posts = this.postsSubject.getValue();
				this.postsSubject.next([...posts, post]);
				this.currentMessage = '';
			});
	}

	public deleteEntity(id: number, type) {
		const entitySubject = type === 'post' ? this.postsSubject : this.usersSubject;

		const entities = entitySubject.getValue();
		const avaialblePosts = entities.filter(post => post.id !== id);
		entitySubject.next(avaialblePosts);
	}

	loadEntities(type, order, field, offset, limit) {
		type === 'posts' ? this.getPosts({ order_by: order, sort: field, offset, limit })
		: this.getUsers({ order_by: order, sort: field, offset, limit });
	}
}
