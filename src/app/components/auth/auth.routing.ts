import { Routes } from '@angular/router';
import { AuthComponent } from './auth.component';

export const AuthPageRoutes: Routes = [
  {path: '', redirectTo: 'auth', pathMatch: 'full'},
  {path: 'auth', component: AuthComponent}];
