import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthApiService } from '../../services/api/auth.api.service';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';

@Component({
	selector: 'app-auth',
	templateUrl: './auth.component.html',
	styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

	public form: FormGroup;
	public isAuthError: boolean;
	public isSignIn = true;
	public error: IResponse;

	constructor(private authApiService: AuthApiService, private router: Router, public authService: AuthService) { }

	ngOnInit() {
		this.initForm();
	}

	private initForm() {
		this.form = new FormGroup({
			email: new FormControl('da9@tut.by', [Validators.required]),
			password: new FormControl('12345678', [Validators.required])
		});
	}

	localSignin() {
		this.isAuthError = false;
		this.error = null;
		this.authApiService.localSignIn({
			email: this.form.value.email,
			password: this.form.value.password
		}).subscribe((res: IResponse) => {
			const token = res.data.access_token;
			localStorage.setItem('access_token', token);

			this.router.navigate(['main/dashboard']);
		}, err => {
			this.isAuthError = true;
			this.error = err.error;
		});
	}

	localSignUp() {
		this.isAuthError = false;
		this.error = null;
		this.authApiService.localSignUp({
			email: this.form.value.email,
			password: this.form.value.password
		}).subscribe(res => {
		}, err => {
			this.isAuthError = true;
			this.error = err.error;
		});
	}

	changeStateOfAuth() {
		this.isSignIn = !this.isSignIn;
		this.isAuthError = false;
		this.error = null;
	}

}
