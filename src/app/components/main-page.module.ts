import { NgModule } from '@angular/core';
import { MainPageComponent } from './main-page.component';
import { RouterModule } from '@angular/router';
import { MainPageRoutes } from './main-page.routing';
import { BrowserModule } from '@angular/platform-browser';
import { DashboardPageModule } from './dashboard/dashboard.module';
import { UsersComponent } from './users/users.component';
import { NavigationModule } from '../shared/component/navigation/navigation.module';
import { PostsComponent } from './posts/posts.component';


@NgModule({
	declarations: [
		MainPageComponent
	],
	imports: [
		BrowserModule,
		DashboardPageModule,
		RouterModule,
		NavigationModule,
		RouterModule.forChild(MainPageRoutes),
	],
	providers: [],
})
export class MainPageModule {
}
