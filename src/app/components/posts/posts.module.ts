import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { PostsComponent } from './posts.component';

@NgModule({
	declarations: [
		PostsComponent
	],
	imports: [
		CommonModule,
		FormsModule
	],
	providers: [],
	exports: [PostsComponent]
})
export class PostsPageModule {
}
