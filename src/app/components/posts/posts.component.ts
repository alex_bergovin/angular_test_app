import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { PostsApiService } from '../../services/api/posts.api.service';

@Component({
	selector: 'app-posts',
	templateUrl: './posts.component.html',
	styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit {
	@Input() id: number;
	@Input() message: string;
	@Input() createdAt: string;
	@Input() user: any;
	@Input() rating: number;
	@Output() addNicknameToChat = new EventEmitter();
	@Output() deletePostEvent = new EventEmitter();
	public isEdit: boolean;
	public tempMessage: string;

	constructor(public authService: AuthService, private postsApiService: PostsApiService) { }

	ngOnInit() {
	}

	addNick() {
		this.addNicknameToChat.emit(this.user.email);
	}

	deletePost() {
		this.postsApiService
			.deletePost(this.id).subscribe(res => {
				this.deletePostEvent.emit(this.id);
			});
	}

	onEdit() {
		this.isEdit = true;
		this.tempMessage = this.message;
	}

	savePost(message, rating = 0) {
		const payload = JSON.parse(JSON.stringify({ message, rating: rating }));
		this.postsApiService.editPost(this.id, payload)
			.subscribe(res => {
				this.message = message ? message : this.message;
				this.isEdit = false;
				this.rating = this.rating + rating;
			});
	}

	changeRating(value) {
		this.savePost(undefined, value);
	}
}
